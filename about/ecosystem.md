@def title = "Ecosystem participants"

# Our Corporate Members

- \fig{./corporate-IFITT.png}
- \fig{./corporate-STI2.png}
- \fig{./corporate-HSR.png}
- \fig{./corporate-PS.png}