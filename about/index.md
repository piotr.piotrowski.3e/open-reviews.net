@def title = "Vision and Goals"

# Our Vision

As consumer choice proliferates, people will rely even more on online reviews to inform their decisions and navigate offerings. Reviews have become a powerful tool and valuable source of insight for individuals and businesses alike. 

The problem we have today is that hundreds of millions of people are feeding their valuable insights currently into proprietary data silos of a few dominant platforms, where they cannot be used for the public good.

The Open Reviews Association is a non-profit initiative to change that. Our vision is a future in which everyone can share and make use of reviews freely. We build open-source [**technology**](/technology) to enable the creation of an **Open Data Ecosystem for Reviews**, with the following guiding principles in mind:

- Anyone can read and write reviews without censorship or monetary restrictions.
- Anyone is free to develop different algorithms (including no algorithm) that help them make sense of the reviews. This includes both non-profit and for-profit purposes.
- Anyone can develop applications which leverage the generic infrastructure while being well suited to the needs of their community.
- The reviews infrastructure and dataset is hosted and maintained by a non-profit organisation. 

Read below why reviews matter and why they should be open, as well as how we intend to achieve that with an Open Data Ecosystem for Reviews. Find out [how you can join and benefit from the Ecosystem](#how_to_participate).

## Building an Open Data Ecosystem for Reviews

### Why online customer reviews matter
* A majority of consumers actively seek out reviews before making a purchase decision, as reviews elicit high credibility and reliance in comparison with information provided by third party editors or advertisements 
* Reviews influence online reputation, discoverability (SEO), and economic success of businesses 
* Reviews are a valuable information-based asset for gaining insights about customers and competitors, and to drive and measure operational improvements
* Reviews provide a qualitative information layer that allows for recommendations based on available factual data

### Why it is a problem that reviews are kept proprietary and fragmented across data silos
* **Barriers for research**: while reviews represent big data, the datasets made available to researchers are mostly fragmented and small in size, disallowing the effective use of state-of-the-art analysis methods, or comparisons across platforms, or across time
* **Barriers for entrepreneurs**: experimentation with technologies that facilitate human decision-making by predicting and surfacing relevant information based on reviews is limited to the proprietary platforms, thereby further increasing monopolistic structures
* **Increasing dependence** of hospitality service providers on dominant proprietary reviews services leads to exploitation (increasing commissions for OTAs, forcing businesses to pay for ads, increasing costs to display reviews on own websites)
* Lack of access to the data creates **inefficiencies** for hospitality service providers (high-effort reputation management across different platforms, no ability to analyse reviews systematically to drive operational improvement, no re-use on own website)
* Despite being crowd-sourced, reviews **cannot be used for the public good** (e.g., for smart cities, smart tourism ecosystems), losing out on opportunities to analyse real-time visitor and resident sentiment

### Ecosystem architecture
Our **vision** is a future in which everyone can share and make use of reviews freely.

To get there we need to **maximise the number of reviews** that people share in an open dataset versus proprietary data silos, **while ensuring** that the dataset remains open, and the privacy requirements of reviewers are respected.

The proposed **way to achieve this goal** is to **create an open data ecosystem for reviews** in a collaborative global effort of researchers, businesses and associations, and smart city actors. If successful, it will not only solve the issues above, but will also create new opportunities for innovation, entrepreneurial activity, and better services. 

> *IBM report [Expanding Innovation: It Takes an Ecosystem](https://www.ibm.com/thought-leadership/institute-business-value/report/innovating-ecosystems#) on how high engagement in ecosystems facilitates innovation, lower costs, and higher revenue.*

\fig{./ecosystem.png}

We have developed open-source infrastructure called **Mangrove** that allows to create and maintain the dataset of open reviews, for everyone to be able to use it freely. [Learn more about **Mangrove** and how you can use the technology](/technology).

To allow interested parties to organize and collaborate effectively towards achieving our joint vision, we created the **Open Reviews Association**. [Learn more about the association](/about/organisation).

### How to participate
The Open Reviews Association has been started with the vision to solve a pressing issue by developing new technology, making it openly available, and by facilitating the creation of a community to collaboratively achieve its goal. Anyone interested in contributing to the vision is welcome to [join as a member of the association](https://open-reviews.net/membership/), as a contributor, or as a user. 

#### Tourism associations, DMOs, and Smart City actors
* Provide exposure and advocacy for the idea of an open data ecosystem for reviews
* Help identify tourism and hospitality businesses to participate in proof-of-concept integrations; provide financial support for proofs of concept where needed
* Integrate Mangrove into your websites to enable immediate testing and showcasing
* Integrate Mangrove early on into digital transformation projects in destinations (open data / knowledge graph efforts) 

#### Tourism and hospitality businesses
Integrate the Mangrove infrastructure into your business websites and apps to be able to interact directly with your customers (they will be more likely to book directly on your website if there are reviews, and not leave and book on an OTA).

#### Navigation and Map applications
Integrate Mangrove into your app to allow users to (i) read and write reviews so they don't have to leave your app when seeking reviews, and to (ii) enable your app to display search results sorted by popularity, relevance, or other metrics.

#### IT service providers, consultants, and entrepreneurs
* Develop useful extensions and products for tourism businesses and municipalities 
    * Build solutions to mirror the open data reviews onto proprietary reviews services to help hospitality businesses through the transition phase
    * Innovate in natural language processing and translation solutions 
    * Provide software for reviews-based reputation management and tracking of operational improvements
    * Innovate in proof-of-purchase / verified-customer technologies
* Provide consultancy services for integrators

#### Researchers
* Help validate the Mangrove infrastructure (technical standard, cryptographic IDs, …)
* Identify areas in research and industry that will benefit from open access to reviews
* Help promote the project and the dataset among researchers
* Help validate the legal framework and ecosystem architecture
