@def title = "Organisation"

# Organisation

The Open Reviews Association (ORA) is an international not-for-profit organization established to **build a global Open Data Ecosystem for online customer reviews**. To achieve this goal, it supports the adoption, maintenance, and growth of the free and open-source **Mangrove** technology: a software infrastructure for reading/writing reviews in an open dataset that is freely accessible to anyone.

The association fulfills the following key functions:
* As a Swiss-registered association, it acts as a legal entity for the Mangrove open-source technology, and is the custodian for the computer servers and services necessary to host the open dataset.
* It supports and communicates with the working groups, and the association board at times delegates various tasks to the working groups.
* It provides a vehicle for fund-raising to support the vision of the project. 


The association invites industry practitioners, researchers, entrepreneurs, developers, open data enthusiasts, and generally anyone interested in open reviews to join as members and support its cause.

[Become a Member](https://open-reviews.net/membership/)

[Read our Articles of Association](https://open-reviews.net/membership/articles-of-association/)

## Board

ORA has been established in 2020 following the release of the Mangrove open-source software infrastructure. The initial **acting board** of the association is composed of the developers of Mangrove and early contributors to the project. Members vote in the annual General Assembly to elect the board members.

- \fig{./board-dina.png}
- \fig{./board-peter.png}
- \fig{./board-elias.png}
- \fig{./board-juho.png}
